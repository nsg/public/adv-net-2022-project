|Group|Presentation time|
|---|---|
|09	kahn	|  14:45 |
|03	shannon	|  14:45 |
|05	dijkstra	|  15:00 |
|01	turing	|  15:00 |
|04	cerf	|  15:15 |
|17	stoica	|  15:15 |
|14	allen	|  15:30 |
|07	boole	|  15:30 |
|08	hopper	|  16:00 |
|02	wirth	|  16:00 |
|11	lamport	|  16:15 |
|13	rexford	|  16:15 |
|10	knuth	|  16:30 |
|16	shenker	|  16:30 |
|06	hamilton	|  16:45 |
|15	berners-lee	|  16:45 |
